import fromFunction from "callbag-from-function";
import H from "callbag-html";
import "drawing-board";
import { from, fromEvent, identity, merge } from "rxjs";
import { last, map, pluck, scan, startWith, switchMap, tap } from "rxjs/operators";
import "wave-renderer";
import { markup } from "./md";
import { teardown, toObservable } from "./util";
import Prism from "prismjs";

const link =
  (to, label, emitter) =>
    H.a({
      href: to,
      onclick: e =>
        !(e.ctrlKey || e.metaKey) ?
          (e.preventDefault(), emitter(prev => ({ ...prev, location: to }))) :
          null
    }, label);

const nav =
  emitter =>
    H.nav(["about", "code", "résumé"].map(x => link(`/${x}`, x, emitter)));

const { source, emitter } = fromFunction();
const source$ = toObservable(source);

const initState = {
  emitter,
  location: window.location.pathname + window.location.hash
};

const state$ =
  source$.pipe(
    startWith(identity),
    scan((acc, v) =>
      v(acc), initState),
    tap(({ location }) =>
      window.history.pushState(null, null, location)),
  );

const popstate$ =
  fromEvent(window, "popstate").pipe(
    pluck("target", "location", "pathname"),
    map(location => ({ location, emitter })),
  );


const effects$ =
  merge(state$, popstate$).pipe(
    tap(_ => teardown(root)),
    switchMap(({ emitter, location }) => from([
      nav(emitter),
      H.hr(),
      markup(location)
    ]).pipe(
      tap(element => root.appendChild(element)),
      last()
    )),
    tap(_ => Prism.highlightAll())
  );

effects$.subscribe();
