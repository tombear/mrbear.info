<article id="resume">
<section id="contact">

## Contact

e-mail: tom at this domain

</section>
<section id="recent">

## Recent

<dl>
<dt>

### Travel, studies, personal projects
#### Berlin, Nepal, India, Barcelona, Home (UK)

<time>
April 2017 - Present
</time>
</dt>
<dd>

The last couple years I've spent travelling, studying independently and working on
projects of my own.

Some things I've learned:

* "Reactive" programming (ReactiveX, Elm, etc.)
  * several code examples [here](/code)
* Haskell
  * completed the tree-drawing exercise from
  [this paper](https://web.cecs.pdx.edu/~mpj/pubs/springschool95.pdf)
  * wrote my other half's website using [Hakyll](https://jaspervdj.be/hakyll/)
* Audio/DSP fundamentals
  * used in my [sound-drawing experiment](/code#Sound_Drawing)
* Cryptography fundamentals
  * first few sets of the [Cryptopals](https://cryptopals.com/) challenges

</dd>
</dl>
<!--<details><summary><em>And an extended narrative for those interested...</em></summary></details>-->

</section>
<section id="previous">

## Prior

<div class="managed-services">
<dl>
<dt>

### Systems Engineer & Developer
#### A Managed Services Provider

<time>Nov 2015 - April 2017</time></dt>
<dd>

Automation development for core network and server configuration management.

* Open-source DevOps tooling (e.g. Terraform, Ansible and so on); Python/Golang programming, shell scripting.
* Small contribution to OpenStack Kolla, did lots of work with OpenStack deployment.
* Lots of Java and Perl the first year, working on in-house configuration management tools.

</dd> 
</dl>
</div>
<div class="it-services">
<dl>
<dt>

### Service Desk Analyst
#### An IT Services Provider

<time>May 2014 - Oct 2015</time></dt>
<dd>

* First job in professional IT.
* Completed certifications independently, including CCNA.

</dd>
</dl>
</div>
</section>
</article>