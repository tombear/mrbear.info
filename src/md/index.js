import about from "./about.md";
import code from "./code.md";
import resume from "./resume.md";
import links from "./links.md";

export const html = html => {
  const template = document.createElement("template");
  template.innerHTML = html;
  return template.content;
};

export const markup = location => {
  const loc =
    new String(location)
      .replace(/\#.*/, "")
      .replace(/\/$/, "");
  switch (loc) {
    case "/about": return html(about);
    case "/code": return html(code);
    case "/resume": case "/résumé": case "/r%C3%A9sum%C3%A9": return html(resume);
    case "/links": return html(links);
    default:
      window.history.pushState(null, null, "/about");
      return html(about);
  }
};