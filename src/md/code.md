<article id="code">
<section id="Sound_Drawing">

## Sound Drawing Boards, WIP

This is a work in progress, playing with drawing sound.
Ideas for next iterations include:

* Plugging many together
* Using drawn signals to modulate
* Working in the same context as peers over a LAN

The following [web components](https://developer.mozilla.org/en-US/docs/Web/Web_Components) can be played with in this browser window.
Source code linked inline.

### Waveform Drawing Component [source](https://gitlab.com/tombear/drawing-board)

<drawing-board id="db0"></drawing-board>

### Waveform Rendering Component [source](https://gitlab.com/tombear/wave-renderer)

<wave-renderer source-element-id="db0"></wave-renderer>
</section>
<hr>
<section id="Shop_Builder">

## Shop Builder, WIP [source](https://gitlab.com/tombear/svelte-shop-old)

<div class="shop-builder">
<iframe src="https://affectionate-carson-4bbcb9.netlify.com/" class="shop-builder"></iframe>

Here is the only demonstrable code I've done for this so far:
a simple mock-up of a shop front-end using [Svelte](https://svelte.dev/),
which I'd wanted to try out since seeing [this talk](https://www.youtube.com/watch?v=AdNJ3fydeao)
by the benevolent Rich Harris. I've since fallen back to the
["build it yourself"](https://github.com/staltz/callbag-basics#contributing)
mantra.

Anywho, the following is the general plan for this project:

* Vendors specify data models for their items
* Vendors choose pre-built web components (or write their own) pertaining to said item data models
* Vendors generate a client bundle (a website)
* Clients (customers) browse the site, putting items into a cart, entering delivery details etc.
* Clients submit orders as a data blob, e.g. JSON.
* Vendors host an API server listening for orders.
* Upon receiving an order request, an API server validates the order.
* If valid, the API server adds a crypto address to the blob, and then signs the blob e.g. with [JWS](https://en.wikipedia.org/wiki/JSON_Web_Signature)
* The API server then sends the signed blob back to the client, basically saying "I will ship ITEMS to DELIVERY ADDRESS upon receving X CRYPTOCURRENCY into wallet at address ADDRESS".
* If the owner of the public key associated with the signing of the blob is deemed to be trustworthy, then the above is useful.

Something similar would need building for couriers.

</div>
</section>
<hr>
<section id="This_Site">

## This Site [source](https://gitlab.com/tombear/mrbear.info)

No framework, just [stream](https://github.com/callbag/callbag)
[libraries](https://rxjs.dev) and
[commonmark](https://github.com/commonmark/commonmark.js/).
I'm really liking this pattern in place of [Redux](https://redux.js.org/)
or ["The Elm Architecture"](https://github.com/dwyl/learn-elm-architecture-in-javascript):

<pre>
<code class="language-javascript">const state$ =
  source$.pipe(
    startWith(identity),
    scan((acc, v) =>
      v(acc), initState),
  );</code>
</pre>

Which seems a nice and easy version of the
["state monad" of Haskell](https://wiki.haskell.org/State_Monad#Complete_and_Concrete_Example_1).

</section>
<hr>
<section id="The_List_Search_Tool">

## ["The List" Search Tool](https://mrbear.info/the-list/) [source](https://gitlab.com/tombear/the-list)

A simple/rough web UI for querying
["the list"](https://www.theguardian.com/world/2018/jun/20/the-list-europe-migrant-bodycount)
of refugee deaths.

It's not "lightning-fast" - too many DOM nodes and all data processing is
happening within the client, needs a virtual DOM like
[snabbdom](https://github.com/snabbdom/snabbdom) - will maybe fix it up if/when
I have time.

<a href="https://mrbear.info/the-list/"><img class="the-list-pre" src="/assets/images/the-list-preview.png" alt="preview of the tool"></a>
</section>
</article>