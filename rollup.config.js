import commonjs from "rollup-plugin-commonjs";
import nodeResolve from "rollup-plugin-node-resolve";
import { Parser, HtmlRenderer } from "commonmark";
import { createFilter } from 'rollup-pluginutils';

export default {
  input: "./src/main.js",
  output: {
    file: "./public/bundle.js",
    format: "esm",
    sourcemap: true,
    name: "Bundle"
  },
  plugins: [
    nodeResolve(),
    commonjs(),
    commonmark({
      include: [
        "./src/md/**.md"
      ]
    })
  ]
};

function commonmark(options = {}) {
  const filter = createFilter(options.include || ["**/*.md"], options.exclude);
  const { parserOpts, rendererOpts } = options.commonmark || {};
  const parser = new Parser(parserOpts);
  const renderer = new HtmlRenderer(rendererOpts)
  return {
    name: "commonmark",

    transform(md, id) {
      if (!filter(id)) return null;
      const htmlOutput = renderer.render(parser.parse(md));
      return {
        code: `export default ${JSON.stringify(htmlOutput)};`,
        map: { mappings: "" }
      }
    }
  }
}